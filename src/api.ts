import express from "express";
import { body, query, validationResult } from "express-validator";
import { queryMockSky } from "./client";
import { putUserLocation, getUserLocation } from "./database";
import cors from "cors";
import bodyParser from "body-parser";

const specLatitude = { min: -90, max: 90 };
const specLongitude = { min: -180, max: 180 };

export function getApi() {
  const app = express();
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.get("/", async (req, res) => {
    res
      .contentType("text/plain")
      .send("Welcome. Please consult the README for API endpoints");
  });

  app.get("/health", async (req, res) => {
    res.contentType("text/plain").send("OK");
  });

  app.post(
    "/api/location",
    body("userId").exists().isUUID(),
    body("latitude").exists().isFloat(specLatitude),
    body("longitude").exists().isFloat(specLongitude),
    async (req, res) => {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          success: false,
          errors: errors.array(),
        });
      }

      const { userId, latitude, longitude } = req.body as any;
      putUserLocation(userId, { latitude, longitude });
      res.status(201).send();
    }
  );

  app.get(
    "/api/location",
    query("userId").exists().isUUID(),
    async (req, res) => {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          success: false,
          errors: errors.array(),
        });
      }

      const { userId } = req.query as any;
      const location = getUserLocation(userId);

      if (location) {
        res.status(200).send(location);
      } else {
        res
          .status(404)
          .send({ success: false, errors: ["Location not found for userId"] });
      }
    }
  );

  app.get(
    "/api/forecast",
    // validate query parameters
    query("userId").exists().isUUID(),
    async (req, res) => {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          success: false,
          errors: errors.array(),
        });
      }

      const { userId } = req.query as any;

      const userLocation = getUserLocation(userId);
      if (!userLocation) {
        return res.status(404).json({
          success: false,
          errors: [
            "User " +
              userId +
              " has no stored location. Please set location before forecast.",
          ],
        });
      }

      const result = await queryMockSky(
        userLocation.latitude,
        userLocation.longitude
      );
      res.status(200).send(result);
    }
  );

  return app;
}
