import debug from "debug";
import { getApi } from "./api";
const log = debug("spark:api");
const port = process.env.PORT_API ?? 3001;
const app = getApi();

// start the Express api server
app.listen(port, () => {
  log(`API Server started at http://localhost:${port}`);
});
