import axios from "axios";
import debug from "debug";
import {
  endOfToday,
  differenceInHours,
  isBefore,
  addHours,
  endOfDay,
} from "date-fns";

const log = debug("spark:client");
const baseUrl = "https://api.darksky.net";
const key = process.env.API_KEY_DARKSKY;

export type TemperatureTime = {
  temperature: number;
  date: Date;
};

export type DarkSkyResult = {
  summary: string;
  windspeed: number;
  temperatureTimes: TemperatureTime[];
};

export type DarkSkyResponse = {
  status: number;
  error?: string;
  body?: string;
};

/**
 * The original task of querying darksky cannot be completed as the API KEY has expired. Instead this mock endpoint returns fixed results.
 */
export async function queryMockSky(
  latitude: number,
  longitude: number,
  now: Date = new Date()
): Promise<DarkSkyResult> {
  // in production we would lookup lat long time zone for user, here we just use machine time
  const temperatureTimes: TemperatureTime[] = [];
  const end = endOfDay(now);
  let cursor = now;

  // build array of times and temperatures
  while (isBefore(cursor, end)) {
    cursor = addHours(cursor, 1);
    temperatureTimes.push({ date: cursor, temperature: 14 });
  }

  return {
    summary: "Fine, with showers at times.",
    windspeed: 12,
    temperatureTimes,
  };
}

/**
 * Query DarkSky API for weather results
 * @deprecated - the provided darksky api key is no longer valid
 */
export async function queryDarkSky(
  latitude: number,
  longitude: number,
  unit: string = "si"
): Promise<DarkSkyResponse> {
  if (!key) {
    throw new Error("Set missing API_KEY_DARKSKY environment variable");
  }
  const url = `${baseUrl}/forecast/${key}/${latitude},${longitude}?units=${unit}`;
  log(`Query darksky (${latitude}, ${longitude}): ${url}`);
  return axios
    .get(url, { timeout: 15000 })
    .then((res) => {
      return {
        status: res.status,
        body: JSON.stringify(res.data),
      };
    })
    .catch((error) => {
      const message = error.message ?? "Unknown error occurred";
      if (error.response) {
        return {
          status: error.response.status,
          error: message,
        };
      } else {
        return {
          error: message,
          status: 500,
        };
      }
    });
}
