import debug from "debug";
const log = debug("spark:api");

// use simple in memory map for demonstration (would use an ORM and DB for production)
const dbUserLocation = new Map<string, UserLocation>();
const dbWeatherCache = new Map<string, string>();

export type UserLocation = {
  latitude: number;
  longitude: number;
};

export function putUserLocation(userId: string, location: UserLocation) {
  log(
    "PUT user location " +
      userId +
      " (" +
      location.latitude +
      ", " +
      location.longitude +
      ")"
  );
  dbUserLocation.set(userId, location);
}

export function getUserLocation(userId: string): UserLocation | undefined {
  log("GET user location " + userId);
  return dbUserLocation.get(userId);
}
