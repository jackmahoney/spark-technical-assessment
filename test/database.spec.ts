import { getUserLocation, putUserLocation } from "../src/database";

import { v4 as uuidv4 } from "uuid";
describe("user location database", () => {
  it("can put new user location", () => {
    const user1 = uuidv4();
    const user2 = uuidv4();
    // at first neither user has location
    expect(getUserLocation(user1)).toBeUndefined();
    expect(getUserLocation(user2)).toBeUndefined();
    // put locations for each
    putUserLocation(user1, { latitude: 1, longitude: 2 });
    putUserLocation(user2, { latitude: 10, longitude: 20 });
    // can read locations
    expect(getUserLocation(user1)).toEqual({ latitude: 1, longitude: 2 });
    expect(getUserLocation(user2)).toEqual({ latitude: 10, longitude: 20 });
  });
});

export {};
