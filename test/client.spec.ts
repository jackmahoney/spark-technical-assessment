import { queryDarkSky, queryMockSky } from "../src/client";
import axios from "axios";
jest.mock("axios");

describe("client", () => {
  it("can provide mock results", async () => {
    const result = await queryMockSky(
      0,
      0,
      new Date(Date.parse("21 May 2020 00:12:00 UTC"))
    );
    expect(result.summary).toBeTruthy();
    expect(result.windspeed).toBeTruthy();
    expect(result.temperatureTimes.length).toBeGreaterThan(0);
  });
  it("can call darksky using axios", async () => {
    (axios.get as any).mockResolvedValue({ status: 200, data: {} });
    const res = await queryDarkSky(10, 20, "si");
    expect(res).toEqual({ body: "{}", status: 200 });
  });
});

export {};
