import { getApi } from "../src/api";
import request from "supertest";
import { v4 as uuidv4 } from "uuid";
import { DarkSkyResult, queryMockSky } from "../src/client";

jest.mock("../src/client", () => {
  const result: DarkSkyResult = {
    summary: "test",
    temperatureTimes: [],
    windspeed: 12,
  };
  return {
    queryMockSky: jest.fn().mockResolvedValue(result),
  };
});

const app = getApi();

describe("basic api endpoints", () => {
  it("has default index", async () => {
    const response = await request(app).get("/");
    expect(response.status).toEqual(200);
    expect(response.text).toContain("Welcome");
  });
  it("has health endpoints", async () => {
    const response = await request(app).get("/health");
    expect(response.status).toEqual(200);
    expect(response.text).toContain("OK");
  });
  it("can handle 404", async () => {
    const response = await request(app).get("/123");
    expect(response.status).toEqual(404);
  });
});
describe("/api/location", () => {
  it("post requires user id", async () => {
    const response = await request(app).post("/api/location");
    expect(response.status).toEqual(400);
  });
  it("get requires user id", async () => {
    const response = await request(app).get("/api/location");
    expect(response.status).toEqual(400);
  });
  it("returns 404 when no location set", async () => {
    const userId = uuidv4();
    const response = await request(app).get("/api/location?userId=" + userId);
    expect(response.status).toEqual(404);
  });
  it("can set and then get location set", async () => {
    const userId = uuidv4();
    const response = await request(app)
      .post("/api/location")
      .send({ userId, latitude: 10, longitude: 20 });

    expect(response.status).toEqual(201);

    const read = await request(app)
      .get("/api/location")
      .query("userId=" + userId);
    expect(read.status).toEqual(200);
    expect(read.body).toEqual({ latitude: 10, longitude: 20 });
  });
});

describe("/api/forecast", () => {
  test("rejects user id ", async () => {
    const response = await request(app).get("/api/forecast");
    expect(response.status).toEqual(400);
  });
  test("can fetch mocked query", async () => {
    const response0 = await request(app)
      .get("/api/forecast")
      .query("userId=" + uuidv4());
    expect(response0.status).toEqual(404);

    const userId = uuidv4();

    const response1 = await request(app)
      .post("/api/location")
      .send({ userId, latitude: 10, longitude: 20 });
    expect(response1.status).toEqual(201);

    const response2 = await request(app)
      .get("/api/forecast")
      .query("userId=" + userId);
    expect(response2.status).toEqual(200);
    expect(response2.body).toEqual({
      summary: "test",
      temperatureTimes: [],
      windspeed: 12,
    });
  });
});

export {};
