import axios from "axios";
import type { NextPage } from "next";
import Head from "next/head";
import { useCallback, useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { DarkSkyResult } from "../src/client";
const base = "http://localhost:3001";

const Home: NextPage = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>();
  const [result, setResult] = useState<DarkSkyResult>();
  const [latitude, setLatitude] = useState<number>(0);
  const [longitude, setLongitude] = useState<number>(0);

  useEffect(() => {
    setUserId(uuidv4());
  }, [setUserId]);

  const loadLocation = useCallback(() => {
    setLoading(true);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        ({ coords = {} }) => {
          if (coords.latitude && coords.longitude) {
            setLongitude(coords.longitude);
            setLatitude(coords.latitude);
            console.log("Coordinates set");
          }
          setLoading(false);
        },
        () => {
          console.error("No geolocation available");
          setLoading(false);
        }
      );
    }
  }, [setLatitude, setLongitude, setLoading]);

  useEffect(() => {
    loadLocation();
  }, [loadLocation]);

  useEffect(() => {
    if (userId) {
      setLoading(true);
      axios
        .post(base + "/api/location", { userId, latitude, longitude })
        .finally(() => setLoading(false));
    }
  }, [latitude, longitude, userId, setLoading]);

  const onSubmit = useCallback(async () => {
    setLoading(true);
    axios
      .get(base + "/api/forecast?userId=" + userId)
      .then((res) => setResult(res.data))
      .finally(() => setLoading(false));
  }, [userId, setResult, setLoading]);
  return (
    <>
      <Head>
        <title>Spark Weather</title>
      </Head>
      <div>
        <div className="bg-white">
          <div className="max-w-7xl mx-auto px-4 py-12 sm:px-6 lg:py-16 lg:px-8">
            <div className="px-6 py-6 bg-indigo-100 rounded-lg md:py-12 md:px-12 lg:py-16 lg:px-16 xl:flex xl:items-center bg-spark">
              <div className="xl:w-0 xl:flex-1">
                <h2 className="text-2xl font-extrabold tracking-tight sm:text-3xl text-violet-800">
                  Live weather updates
                </h2>
                <p className="mt-3 max-w-2xl text-lg leading-6 text-slate-600">
                  Enter your latitude and longitude below or{" "}
                  <span
                    className="underline text-slate-800 font-bold cursor-pointer"
                    onClick={loadLocation}
                  >
                    use your location
                  </span>{" "}
                  to load weather data.
                </p>
              </div>
              <div className="mt-8 sm:w-full sm:max-w-md xl:mt-0 xl:ml-8">
                <form className="flex gap-2 justify-center content-center">
                  <input
                    type="number"
                    required
                    min={-90}
                    max={90}
                    value={latitude}
                    onChange={(e) => setLatitude(parseFloat(e.target.value))}
                    className="w-full border-white px-5 py-3 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-indigo-700 focus:ring-white rounded-md"
                    placeholder="Latitude"
                  />
                  <input
                    type="number"
                    min={-180}
                    max={180}
                    required
                    value={longitude}
                    onChange={(e) => setLongitude(parseFloat(e.target.value))}
                    className="w-full border-white px-5 py-3 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-indigo-700 focus:ring-white rounded-md"
                    placeholder="Longitude"
                  />
                  <button
                    type="submit"
                    onClick={onSubmit}
                    disabled={loading}
                    className="w-full flex items-center justify-center px-5 py-3 border border-transparent shadow text-base font-medium rounded-md text-white bg-indigo-500 hover:bg-indigo-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-indigo-700 focus:ring-white"
                  >
                    Load weather
                  </button>
                </form>
              </div>
            </div>

            <div className="mt-6 px-6 py-6 bg-teal-100 rounded-lg md:py-12 md:px-12 lg:py-16 lg:px-16 xl:flex xl:items-center">
              <div className="xl:w-0 xl:flex-1">
                <h2 className="text-2xl font-extrabold tracking-tight sm:text-3xl text-teal-800">
                  {loading ? "Loading..." : "Results"}
                </h2>
                <div>{JSON.stringify(result)}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
