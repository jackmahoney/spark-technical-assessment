# Spark Technical Assessment 

Weather API for technical assessment. Provides weather report for a given longitude and latitude.

- Has two parts: an express server on port 3001 in src folder, and a next-js frontend on port 3000 in pages folder.
- Makefile for tasks

NOTE: assignment was given with a faulty API KEY that did not grant access to DarkSky API, for that reason I mocked the API and instead tried to demonstrate as much of my code style as possible within a mocked environment. I built a web app and server side by side to demonstrate.

## Structure
- src/ backend api
- test/ jest unit tests
- pages/ front-end
- Makefile

## Install

### Linux / OSX

- Install NodeJS >= 14
- Run `make` (or `npm install`)

### Windows
Try the linux commands in WSL or build the docker image.

## Test

- Run `make test`

## Start server

- Run `make start` or `make dev`.

Then load the web app in a browser at `http://localhost:3000` or call the api endpoints on `http://localhost:3001`.

## Specification

### User story
- User provides longitude and latitude values to forecast endpoint
- This is stored against a user for subsequent requests
- Endpoint returns:
-- summary string of current weather
-- wind speed and direction
-- array of temperatures for timestamps during a day

### Endpoints
- POST /api/location
-- Accepts long and lat and stores against user
- GET /api/forecast
-- Queries upstream DarkSky API using stored location
