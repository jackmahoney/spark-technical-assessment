module.exports = {
  moduleFileExtensions: ['ts','js'],
  transform: {
    '^.+\\.[tj]sx?$': 'ts-jest',
  },
  testRegex: '/test/.+.spec.(t|j)sx?$',
  reporters: ['default', ['jest-junit', { outputDirectory: 'test-results' }]],
  testResultsProcessor: 'jest-junit',
  testURL: 'https://localhost:3000/',
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.ts',
    '!<rootDir>/src/index.ts',
  ],
  coverageReporters: ['text', 'html']
};
