node_modules:
	npm ci

dev: node_modules build_api
	$(ENV) npm run dev

# tasks
start: build_api build_web
	$(ENV) npm run start

start_api: build_api
	$(ENV) npm run start:api

build_web: node_modules
	$(ENV) npm run build:web

build_api: node_modules
	$(ENV) npm run build:api

start_web: node_modules .next
	$(ENV) npm run start:web

.next: build_api

fmt: node_modules
	npm run fmt

test: node_modules
	$(ENV) npm run test

docker:
	docker build -t spark-weather .

# environment variables
DEBUG=spark*
API_KEY_DARKSKY=5bd31fc5ee7b0a2aa3df023338e856eb # would store in ci/infra for production
PORT_API=3001
PORT_WEB=3000
ENV=DEBUG=$(DEBUG) PORT_API=$(PORT_API) PORT=$(PORT_WEB) API_KEY_DARKSKY=$(API_KEY_DARKSKY)

.PHONY: test